# 环境安装指南
[AWS服务器](./README-AWS.md)|其他服务器  

## 准备
1. 创建5台服务器(`bastion 1` ,`master 1(etcd 共用) `,`node 3`) **ubuntu 16.04 2CPU 4G Memory**（bastion的内存最小可以2G）
2. 根据具体的密匙信息，请在 `files/manifest/variable.json` 文件中进行配置 (除了bastion host，其它的node配置内网地址)
3. 将本机的`~/.ssh/id_rsa.pub` 添加到 `bastion` 的 `vim ~/.ssh/authorized_keys`（如果没有创建一个）
4. 在本地电脑 hosts 中添加 
```
{BASTION_IP}    experience.thoughtworks.cn
{MASTER_IP}     jenkins.experience.thoughtworks.cn
```
其中 {BASTION_IP}、{BASTION_IP} 分别是 bastion、master 服务器IP 
5. 手动在 `bastion` 服务器中install  `jq`
```
sudo apt-get update
sudo apt install jq
```
6. 开启`bastion`服务器、`master`服务器相应端口

## 启动 & 安装
- **方式1：一键部署**  
  进入到项目根据目录下执行 `./remote all` 即可  
    
- **方式2：分步部署**  
  1. `./remote copy_files` 复制脚本文件到 bastion服务器中
  2. `./remote setup_env` 初始化环境
  3. `./remote setup_k8s` 初始化k8s配置
  4. `./remote deploy_services` 部署所有服务 


## Kubernetes dashboard 操作
 - `{MASTER_NODE_IP}`: 是 master 服务器的IP
 - `{DASHBOARD_PORT}`: 获取 `DASHBOARD_PORT`
    ```
    在项目跟目录下执行
     ./remote login
     进入到 bastion server，继续执行
     kubectl get svc --all-namespaces 
     找到 name 为 kubernetes-dashboard 这一行获取到端口号
    ```
1. 登录`dashboard https://{MASTER_NODE_IP}:{DASHBOARD_PORT}`
2. 要求输入`token`，获取方法
  
   ```
   # 本地
   ./remote token
   # 复制 token 开头的字符串到 登录界面 token 字段中即可
   ```
   
3. 进入dashboard界面，即可对常规资源，主要是POD等资源的各种操作，如查看日志等。

   - 选择 namespaces
   - 选择POD，查看日志等


## 数据库相关操作

1. 执行完「启动 & 安装」步骤后，数据库即安装成功，schema 也初始化成功。

2. 所有数据库 `ip：{BASTION_IP} `用户名：`root`，密码：`{MYSQL_ROOT_PASSWORD}`
 其中 ：
  - `{MYSQL_ROOT_PASSWORD}` 为 `files/manifest/variable.json` 中配置的 `mysql_root_password`   
  - `{BASTION_IP}` :为 bastion 服务器的 IP


## Tips
1. 请不要修改 `variable.json` 文件中的 `mysql_root_passwrod` 的值

## [常见问题](./QUESTION.md)