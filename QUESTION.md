# 部署常见问题列表

返回脚本：[AWS服务器](./README-AWS.md)|[其他服务器](./README.md)

---

1. 提示mysql连接失败？  

解决方案：  
- 打开配置文件：`vim /etc/mysql/mysql.conf.d/mysqld.cnf`或者`vim /etc/mysql/my.cnf`.
- 注释掉该配置: `bind-address=127.0.0.1`.
- 重启服务: `/etc/init.d/mysql restart`.

---
2. 出简单编程题目时提示没有权限？

解决方案：  
登录到 `Kubernetes dashboard` -》 `Namespace:production`
 -》`Pods` -》 `tws-quiz-center-*` -》`EXEC` -》`tws-quiz-center-jenkins` -》 执行命令`sudo chmod -R 777 var/jenkins_home/` 