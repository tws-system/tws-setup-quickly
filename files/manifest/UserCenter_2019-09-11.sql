

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

use UserCenter;
# Dump of table emailReset
# ------------------------------------------------------------

DROP TABLE IF EXISTS `emailReset`;

CREATE TABLE `emailReset` (
  `uuid` varchar(128) COLLATE utf8_bin NOT NULL,
  `userId` bigint(11) NOT NULL,
  `email` varchar(128) COLLATE utf8_bin NOT NULL,
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('SUCCESS','PENDING','FAILURE') COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table nodeBBUser
# ------------------------------------------------------------

DROP TABLE IF EXISTS `nodeBBUser`;

CREATE TABLE `nodeBBUser` (
  `username` varchar(128) COLLATE utf8_bin NOT NULL,
  `password` varchar(128) COLLATE utf8_bin NOT NULL,
  `userId` int(11) NOT NULL,
  `createDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `fk_user_id_3` (`userId`),
  CONSTRAINT `fk_user_id_3` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table notification
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notification`;

CREATE TABLE `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `senderId` int(11) DEFAULT NULL,
  `receiverId` int(11) DEFAULT NULL,
  `isRead` tinyint(1) DEFAULT '0',
  `message` varchar(1000) DEFAULT NULL,
  `url` varchar(500) DEFAULT NULL,
  `createTime` date DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `message_en` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table passwordReset
# ------------------------------------------------------------

DROP TABLE IF EXISTS `passwordReset`;

CREATE TABLE `passwordReset` (
  `uuid` varchar(128) COLLATE utf8_bin NOT NULL,
  `userId` int(11) NOT NULL,
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isVaild` varchar(128) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table schema_version
# ------------------------------------------------------------

DROP TABLE IF EXISTS `schema_version`;

CREATE TABLE `schema_version` (
  `installed_rank` int(11) NOT NULL,
  `version` varchar(50) DEFAULT NULL,
  `description` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL,
  `script` varchar(1000) NOT NULL,
  `checksum` int(11) DEFAULT NULL,
  `installed_by` varchar(100) NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`installed_rank`),
  KEY `schema_version_s_idx` (`success`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table socialAccount
# ------------------------------------------------------------

DROP TABLE IF EXISTS `socialAccount`;

CREATE TABLE `socialAccount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `type` varchar(128) COLLATE utf8_bin NOT NULL,
  `displayName` varchar(128) COLLATE utf8_bin NOT NULL,
  `openid` varchar(128) COLLATE utf8_bin NOT NULL,
  `unionid` varchar(128) COLLATE utf8_bin NOT NULL,
  `imageUrl` varchar(256) COLLATE utf8_bin NOT NULL,
  `accessToken` varchar(256) COLLATE utf8_bin NOT NULL,
  `refreshToken` varchar(256) COLLATE utf8_bin NOT NULL,
  `expireTime` int(11) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) COLLATE utf8_bin NOT NULL,
  `mobilePhone` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `password` varchar(128) COLLATE utf8_bin NOT NULL,
  `userName` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `createDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `areaCode` int(11) DEFAULT '86',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `userName` (`userName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table UserConnection
# ------------------------------------------------------------

DROP TABLE IF EXISTS `UserConnection`;

CREATE TABLE `UserConnection` (
  `userId` varchar(255) NOT NULL,
  `providerId` varchar(255) NOT NULL,
  `providerUserId` varchar(255) NOT NULL,
  `rank` int(11) NOT NULL,
  `displayName` varchar(255) DEFAULT NULL,
  `profileUrl` varchar(512) DEFAULT NULL,
  `imageUrl` varchar(512) DEFAULT NULL,
  `accessToken` varchar(512) NOT NULL,
  `secret` varchar(512) DEFAULT NULL,
  `refreshToken` varchar(512) DEFAULT NULL,
  `expireTime` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`userId`,`providerId`,`providerUserId`),
  UNIQUE KEY `UserConnectionRank` (`userId`,`providerId`,`rank`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table userDetail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `userDetail`;

CREATE TABLE `userDetail` (
  `userId` int(11) NOT NULL,
  `school` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(128) COLLATE utf8_bin NOT NULL,
  `major` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `degree` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `gender` enum('F','M') COLLATE utf8_bin DEFAULT NULL,
  `schoolProvince` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `schoolCity` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `entranceYear` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `wechat` varchar(50) COLLATE utf8_bin DEFAULT '',
  `qq` varchar(50) COLLATE utf8_bin DEFAULT '',
  `currentOrganizationId` int(11) DEFAULT '1',
  UNIQUE KEY `userId` (`userId`),
  CONSTRAINT `fk_user_id` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table userProgram
# ------------------------------------------------------------

DROP TABLE IF EXISTS `userProgram`;

CREATE TABLE `userProgram` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) COLLATE utf8_bin NOT NULL,
  `mobilePhone` varchar(64) COLLATE utf8_bin NOT NULL,
  `password` varchar(128) COLLATE utf8_bin NOT NULL,
  `createDate` int(11) NOT NULL,
  `userName` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table userRole
# ------------------------------------------------------------

DROP TABLE IF EXISTS `userRole`;

CREATE TABLE `userRole` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `role` int(7) NOT NULL,
  `organizationId` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) COLLATE utf8_bin NOT NULL,
  `mobilePhone` varchar(64) COLLATE utf8_bin NOT NULL,
  `password` varchar(128) COLLATE utf8_bin NOT NULL,
  `createDate` int(11) NOT NULL,
  `userName` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


INSERT INTO `schema_version` (`installed_rank`, `version`, `description`, `type`, `script`, `checksum`, `installed_by`, `installed_on`, `execution_time`, `success`)
VALUES
	(1,'1','Create user table structure','SQL','V1__Create_user_table_structure.sql',1165008760,'UserCenter','2017-09-21 03:18:02',19,1),
	(2,'2','Create user role table structure','SQL','V2__Create_user_role_table_structure.sql',-1876400556,'UserCenter','2017-09-21 03:18:02',14,1),
	(3,'3','Create user program table structure','SQL','V3__Create_user_program_table_structure.sql',2118555716,'UserCenter','2017-09-21 03:18:02',12,1),
	(4,'4','Create nodebb user table structure','SQL','V4__Create_nodebb_user_table_structure.sql',1392439081,'UserCenter','2017-09-21 03:18:02',12,1),
	(5,'5','Add constraint for user table','SQL','V5__Add_constraint_for_user_table.sql',-741293180,'UserCenter','2017-09-21 03:18:02',42,1),
	(6,'6','Modify user table change createDate','SQL','V6__Modify_user_table_change_createDate.sql',191064758,'UserCenter','2017-09-21 03:18:02',71,1),
	(7,'7','Modify nodebb user table change createDate','SQL','V7__Modify_nodebb_user_table_change_createDate.sql',-1540214605,'UserCenter','2017-09-21 03:18:02',27,1),
	(8,'8','Create user detail table','SQL','V8__Create_user_detail_table.sql',641458765,'UserCenter','2017-09-21 03:18:02',14,1),
	(9,'9','Create user role table','SQL','V9__Create_user_role_table.sql',115329669,'UserCenter','2017-09-21 03:18:02',22,1),
	(10,'10','Add-constraint for nodebb user','SQL','V10__Add-constraint_for_nodebb_user.sql',853900043,'UserCenter','2017-09-21 03:18:02',58,1),
	(11,'11','Add-id in user role','SQL','V11__Add-id_in_user_role.sql',-1607978930,'UserCenter','2017-10-19 03:50:56',87,1),
	(12,'12','Create password reset table structure','SQL','V12__Create_password_reset_table_structure.sql',-1850379588,'UserCenter','2017-10-19 03:50:56',12,1),
	(13,'13','Create email reset table structure','SQL','V13__Create_email_reset_table_structure.sql',-2112067276,'UserCenter','2018-02-07 06:14:26',390,1),
	(14,'14','Alter email reset table structure','SQL','V14__Alter_email_reset_colmun.sql',-39302454,'UserCenter','2018-02-07 14:39:51',111,1),
	(15,'15','Add wechat and qq columns in user detail table','SQL','V15__Add_wechat_and_qq_columns_in_user_detail_table.sql',-1545865221,'UserCenter','2018-08-06 02:49:02',351,1),
	(16,'16','create notification table','SQL','V16__create_notification_table.sql',-799132571,'UserCenter','2018-08-22 03:00:10',24,1),
	(17,'17','divide designer role','SQL','V17__divide_designer_role.sql',-113603056,'UserCenter','2018-10-23 09:43:00',167,1),
	(18,'18','Create social account table','SQL','V18__Create_social_account_table.sql',-1258457130,'UserCenter','2018-10-26 09:50:20',101,1),
	(19,'19','add areaCode column in user table','SQL','V19__add_areaCode_column_in_user_table.sql',-659739800,'UserCenter','2018-10-31 01:54:29',243,1),
	(20,'20','add message en column in notification table','SQL','V20__add_message_en_column_in_notification_table.sql',-1395916527,'UserCenter','2018-11-02 05:51:40',77,1),
	(21,'21','migrate wecaht info to social account table','SQL','V21__migrate_wecaht_info_to_social_account_table.sql',265789270,'UserCenter','2018-11-02 19:38:42',32,1),
	(22,'22','Alter userDetail table structure','SQL','V22__Alter_userDetail_table_structure.sql',-858811615,'UserCenter','2018-11-06 07:29:58',173,1),
	(23,'23','add  organizationId in userRole table','SQL','V23__add__organizationId_in_userRole_table.sql',-349330836,'UserCenter','2019-02-13 06:35:03',98,1),
	(24,'24','add  currentOrganizationId in userDetail table','SQL','V24__add__currentOrganizationId_in_userDetail_table.sql',357193054,'UserCenter','2019-02-13 06:35:03',61,1),
	(25,'25','delete user role unique','SQL','V25__delete_user_role_unique.sql',-568148029,'UserCenter','2019-02-14 09:42:33',34,1);



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
