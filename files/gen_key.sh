#!/bin/bash
set -ex

ssh-keygen -t rsa -b 4096 -C "thoughtworks@thoughtworks.com"