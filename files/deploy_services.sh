#!/bin/bash
set -e
BASTION_HOST=$(sh ./const.sh "$(pwd)/manifest/variable.json" "host.bastion.ip" | sed 's/\"//g' )

kubectl apply -f ./manifest/service-deployment-files/tws-zuul.yml

sed -i "s/{MYSQL_SERVER}/$BASTION_HOST/g" ./manifest/service-deployment-files/tws-quiz-center.yml
kubectl apply -f ./manifest/service-deployment-files/tws-quiz-center.yml

sed -i "s/{MYSQL_SERVER}/$BASTION_HOST/g" ./manifest/service-deployment-files/tws-auth-center.yml
kubectl apply -f ./manifest/service-deployment-files/tws-auth-center.yml

sed -i "s/{MYSQL_SERVER}/$BASTION_HOST/g" ./manifest/service-deployment-files/tws-organization-center.yml
kubectl apply -f ./manifest/service-deployment-files/tws-organization-center.yml

sed -i "s/{MYSQL_SERVER}/$BASTION_HOST/g" ./manifest/service-deployment-files/tws-diff.yml
kubectl apply -f ./manifest/service-deployment-files/tws-diff.yml

sed -i "s/{MYSQL_SERVER}/$BASTION_HOST/g" ./manifest/service-deployment-files/tws-user-center.yml
kubectl apply -f ./manifest/service-deployment-files/tws-user-center.yml

sed -i "s/{MYSQL_SERVER}/$BASTION_HOST/g" ./manifest/service-deployment-files/tws-notification.yml
kubectl apply -f ./manifest/service-deployment-files/tws-notification.yml

sed -i "s/{MYSQL_SERVER}/$BASTION_HOST/g" ./manifest/service-deployment-files/growth-note-app.yml
kubectl apply -f ./manifest/service-deployment-files/growth-note-app.yml

sed -i "s/{MYSQL_SERVER}/$BASTION_HOST/g" ./manifest/service-deployment-files/tws-course-center.yml
kubectl apply -f ./manifest/service-deployment-files/tws-course-center.yml

kubectl apply -f ./manifest/service-deployment-files/thoughtworks-school-react.yml
kubectl apply -f ./manifest/service-deployment-files/tws-auth-center-web.yml
kubectl apply -f ./manifest/service-deployment-files/tws-instructor-web.yml
kubectl apply -f ./manifest/service-deployment-files/tws-tutor-web.yml
kubectl apply -f ./manifest/service-deployment-files/tws-admin-web.yml
kubectl apply -f ./manifest/service-deployment-files/tws-survey-web.yml
kubectl apply -f ./manifest/service-deployment-files/tws-online-language-quiz-web.yml
kubectl apply -f ./manifest/service-deployment-files/tws-student-web.yml
kubectl apply -f ./manifest/service-deployment-files/tws-logic-quiz-web.yml
kubectl apply -f ./manifest/service-deployment-files/tws-simple-coding-quiz-web.yml