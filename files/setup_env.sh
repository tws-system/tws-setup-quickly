#!/bin/bash

set -e
HOSTS=$(sh ./const.sh "$(pwd)/manifest/variable.json" "host")
MASTER_NODE=$(echo $HOSTS | jq '.master')
BASTION_NODE=$(echo $HOSTS | jq '.bastion')
WORK_NODES=$(echo $HOSTS | jq '.work_nodes')


setup_server(){
		sudo su <<EOF
		# 1. 在 bastion server 上gen ssh key(root用户)(if not exist) 
		if [ ! -f "/root/.ssh/id_rsa.pub" ]; then 
			sudo sh ./gen_key.sh
		fi
		# 2. install sshpass for ssh to other server with password on one line
		apt install sshpass
EOF

		# 3. 将(root 用户) public key 放入到 其它server(root 用户)的 ~/.ssh/authorized_keys 中
		PUBLIC_RSA_KEY=$(sudo cat /root/.ssh/id_rsa.pub)
			# master node
		sudo sshpass -p $(echo "${MASTER_NODE}" |  jq '.password' | sed 's/\"//g') ssh $(echo "${MASTER_NODE}" |  jq '.username' | sed 's/\"//g')@$(echo "${MASTER_NODE}" |  jq '.ip'  | sed 's/\"//g') /bin/bash << EOF
				sudo echo $PUBLIC_RSA_KEY >> ~/.ssh/authorized_keys
EOF
			# work node
		for row in $(echo "${WORK_NODES}" | jq -r '.[] | @base64'); do
			_jq() {
				echo ${row} | base64 --decode | jq -r ${1}
			}
			sudo sshpass -p $(_jq '.password') ssh $(_jq '.username')@$(_jq '.ip') /bin/bash << EOF
				sudo echo $PUBLIC_RSA_KEY >> ~/.ssh/authorized_keys
EOF
		done
	# 4. waiting apt-update over
	waiting_apt_updating
	
	# 5. install_base_soft
	install_base_soft
}

setup_python_env(){
	_install(){
		username=$1
		ip=$2
		echo $1 
		echo $2
		sudo ssh $username@$ip /bin/bash<< EOF
				echo '************ start init python env at $ip ************'
				sudo apt-get update 
				sudo apt-get upgrade -y
				sudo apt-get dist-upgrade -y
				sudo apt-get install python2.7 -y
				sudo ln -s /usr/bin/python2.7 /usr/bin/python
				export LC_ALL=C
				sudo apt-get install git python-pip -y
				sudo pip install pip --upgrade -i https://mirrors.aliyun.com/pypi/simple/
EOF
	}
	
	echo '************ start init python env ************'
	_install $(echo "${MASTER_NODE}" |  jq '.username' | sed 's/\"//g') $(echo "${MASTER_NODE}" |  jq '.ip' | sed 's/\"//g')
	for row in $(echo "${WORK_NODES}" | jq -r '.[] | @base64'); do
			_jq() {
				echo ${row} | base64 --decode | jq -r ${1}
			}
			_install $(_jq '.username') $(_jq '.ip')
	done
}

setup_config(){
	# 1. 替换 files/hosts/manifest/hosts 文件中的变量（${XXX_NODE})
	MASTER_IP=$(echo $MASTER_NODE | jq '.ip' | sed 's/\"//g')
	BASTION_NODE_IP=$(echo $BASTION_NODE | jq '.ip' | sed 's/\"//g')
	WORKS_IP=''
	for row in $(echo "${WORK_NODES}" | jq -r '.[] | @base64'); do
		_jq() {
			echo ${row} | base64 --decode | jq -r ${1}
		}
		WORKS_IP=$WORKS_IP$(_jq '.ip')\\n
	done
	sed -i "s/{MASTER_NODE}/$MASTER_IP/g" ./manifest/hosts
	sed -i "s/{ETCD_NODE}/$MASTER_IP/g" ./manifest/hosts
	sed -i "s/{WORK_NODE}/$WORKS_IP/g" ./manifest/hosts
	sed -i "s/{PV_SERVER}/$BASTION_NODE_IP/g" ./manifest/service-deployment-files/pv.yml
	
	# 2. 替换 files/hosts/manifest/nginx_conf 文件中的变量（${MASTER_IP})
	sed -i "s/{MASTER_IP}/$MASTER_IP/g" ./manifest/nginx-conf

	# 3. 替换 install_mysql_server.sh 中的 MYSQL_ROOT_PASSWORD
	MYSQL_ROOT_PASSWORD=$(sh ./const.sh "$(pwd)/manifest/variable.json" "mysql_root_password")
	sed -i "s/{MYSQL_ROOT_PASSWORD}/$MYSQL_ROOT_PASSWORD/g" ./install_mysql_server.sh
}


waiting_apt_updating(){	
	echo '************ waiting_apt_updating *************'
	i=$(ps -ef | grep apt | grep -v grep | wc -l)
	while [ "$i" -ne 0 ]
	do
		i=$(ps -ef | grep apt | grep -v grep | wc -l)
		echo "waiting system apt process: $i"
		sleep 1
	done
}

install_base_soft(){
	echo '************ start install base soft(MySQL) ************'
  sudo apt-get -y install jq mysql-client-core-5.7
	sh install_mysql_server.sh

	echo '************ start install nginx ************'
	sudo apt install nginx -y
	sudo cp /home/ubuntu/files/manifest/nginx-conf /etc/nginx/conf.d/default.conf
	sudo nginx -s reload
}

setup_config
setup_server
setup_python_env
