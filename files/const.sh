#!/bin/bash

set -e
# $1: variable.json path
# $2: find (data) by $2

cat $1 | jq '.'$2 